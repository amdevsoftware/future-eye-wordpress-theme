<?php

class JmSite extends TimberSite {


    function __construct() {

        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'menus' );

        add_filter( 'timber_context', array( $this, 'add_context_variables' ) );
        add_filter( 'get_twig', array( $this, 'add_twig_helpers' ) );

        /**
         * Enqueue scripts and styles
         */
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue_styles_and_scripts') );

        /**
         * Register menus
         */
        add_action('after_setup_theme', array($this, 'register_wp_menus'));

        /**
         * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
         */
        add_action( 'vc_before_init', array($this, 'jm_vcSetAsTheme') );

        /**
         * Case Study post type
         */
        add_action( 'init', array($this, 'register_case_study_post_type'), 0 );
        add_action( 'init', array($this, 'register_team_post_type'), 0 );
        add_action( 'init', array($this, 'register_team_category'), 0 );

        add_action( 'pre_get_posts', array($this, 'post_per_page_control') );

        parent::__construct();
    }


    function register_wp_menus() {

        register_nav_menu( 'primary_menu', __( 'Primary Menu', THEME_TD ) );
        register_nav_menu( 'footer_menu_1', __( 'Footer Menu 1', THEME_TD ) );
        register_nav_menu( 'footer_menu_2', __( 'Footer Menu 2', THEME_TD ) );
        register_nav_menu( 'footer_menu_3', __( 'Footer Menu 3', THEME_TD ) );
        register_nav_menu( 'footer_menu_4', __( 'Footer Menu 4', THEME_TD ) );
    }


    function enqueue_styles_and_scripts() {

        $theme = wp_get_theme();
        $theme_version = $theme->get( 'Version' );

        global $post;
        $post_slug = "";

        if ( isset($post->post_name)) {
            $post_slug = $post->post_name;
        }

        // CSS

        wp_enqueue_style('bootstrap-css',
            get_template_directory_uri().'/static/css/bootstrap.min.css',
            array(),
            $theme_version
        );

        wp_enqueue_style('main-css',
            get_template_directory_uri(). '/static/css/style.css',
            array('bootstrap-css'),
            $theme_version
        );

        wp_enqueue_style('magnific-popup-css',
            get_template_directory_uri(). '/static/css/magnific-popup.min.css',
            array('bootstrap-css'),
            $theme_version
        );

        // JS

        wp_enqueue_script( 'bootstrap-bundle-min-js',
            get_template_directory_uri() . '/static/js/bootstrap.bundle.min.js',
            array('jquery'),
            $theme_version,
            true
        );

        wp_enqueue_script( 'isotope.pkgd.min.js',
            get_template_directory_uri() . '/static/js/isotope.pkgd.min.js',
            array('jquery'),
            $theme_version,
            true
        );

        wp_enqueue_script( 'magnific-popup.min-js',
            get_template_directory_uri() . '/static/js/jquery.magnific-popup.min.js',
            array('jquery'),
            $theme_version,
            true
        );

        wp_enqueue_script( 'all-js',
            get_template_directory_uri() . '/static/js/all.js',
            array('jquery'),
            $theme_version,
            true
        );

        wp_enqueue_script( 'script-js',
            get_template_directory_uri() . '/static/js/script.js',
            array('jquery'),
            $theme_version,
            true
        );

    }



    function jm_vcSetAsTheme() {
        vc_set_as_theme();
    }

    function get_theme_menu_name( $theme_location ) {
        $theme_locations = get_nav_menu_locations();

        $menu_obj = get_term( $theme_locations[$theme_location], 'nav_menu' );

        return $menu_obj->name;
    }

    function add_context_variables( $context ) {

        // Add theme version variable
        $theme = wp_get_theme();
        $theme_version = $theme->get( 'Version' );
        $context['theme_version'] = $theme_version;

        // All theme links
        $context['why_we_do_it_link'] = site_url('/why-we-do-it/');
        $context['how_we_do_it_link'] = site_url('/how-we-do-it/');
        $context['what_we_do_link'] = site_url('/what-we-do/');
        $context['case_studies_link'] = get_post_type_archive_link('case_study');
        $context['team_link'] = get_post_type_archive_link('team');
        $context['blog_link'] = get_post_type_archive_link('post');
        $context['contact_link'] = site_url('/contact/');
        $context['resources_link'] = site_url('/resources/');
        $context['press_link'] = site_url('/press/');
        $context['privacy_link'] = site_url('/privacy-policy/');


        if ( has_nav_menu( 'primary_menu' ) ) {

            $context['primary_menu'] = new TimberMenu('primary_menu');
        }

        if ( has_nav_menu( 'footer_menu_1' ) ) {

            $context['footer_menu_1'] = new TimberMenu('footer_menu_1');
        }

        if ( has_nav_menu( 'footer_menu_2' ) ) {

            $context['footer_menu_2'] = new TimberMenu('footer_menu_2');
        }

        if ( has_nav_menu( 'footer_menu_3' ) ) {

            $context['footer_menu_3'] = new TimberMenu('footer_menu_3');
        }

        if ( has_nav_menu( 'footer_menu_4' ) ) {

            $context['footer_menu_4'] = new TimberMenu('footer_menu_4');
        }

        $context['site'] = $this;
        return $context;
    }

    // Register Custom Post Type
    function register_case_study_post_type() {

        $labels = array(
            'name'                  => _x( 'Case Studies', 'Post Type General Name', THEME_TD ),
            'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', THEME_TD ),
            'menu_name'             => __( 'Case Studies', THEME_TD ),
            'name_admin_bar'        => __( 'Case Study', THEME_TD ),
            'archives'              => __( 'Item Archives', THEME_TD ),
            'attributes'            => __( 'Item Attributes', THEME_TD ),
            'parent_item_colon'     => __( 'Parent Item:', THEME_TD ),
            'all_items'             => __( 'All Items', THEME_TD ),
            'add_new_item'          => __( 'Add New Item', THEME_TD ),
            'add_new'               => __( 'Add New', THEME_TD ),
            'new_item'              => __( 'New Item', THEME_TD ),
            'edit_item'             => __( 'Edit Item', THEME_TD ),
            'update_item'           => __( 'Update Item', THEME_TD ),
            'view_item'             => __( 'View Item', THEME_TD ),
            'view_items'            => __( 'View Items', THEME_TD ),
            'search_items'          => __( 'Search Item', THEME_TD ),
            'not_found'             => __( 'Not found', THEME_TD ),
            'not_found_in_trash'    => __( 'Not found in Trash', THEME_TD ),
            'featured_image'        => __( 'Featured Image', THEME_TD ),
            'set_featured_image'    => __( 'Set featured image', THEME_TD ),
            'remove_featured_image' => __( 'Remove featured image', THEME_TD ),
            'use_featured_image'    => __( 'Use as featured image', THEME_TD ),
            'insert_into_item'      => __( 'Insert into item', THEME_TD ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', THEME_TD ),
            'items_list'            => __( 'Items list', THEME_TD ),
            'items_list_navigation' => __( 'Items list navigation', THEME_TD ),
            'filter_items_list'     => __( 'Filter items list', THEME_TD ),
        );
        $rewrite = array(
            'slug'                  => 'case-study',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => false,
        );
        $args = array(
            'label'                 => __( 'Case Study', THEME_TD ),
            'description'           => __( 'This is Сase Study pages', THEME_TD ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'thumbnail', 'revisions', 'editor'),
            'taxonomies'            => array( 'post_tag' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-format-status',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
//            'rewrite'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
        );
        register_post_type( 'case_study', $args );

    }

    function register_team_category() {

        register_taxonomy(
            'case_study-category',
            'case_study',
            array(
                'label' => __( 'Categories' ),
                'rewrite' => array( 'slug' => 'case_study-category' ),
                'hierarchical' => true,
            )
        );
    }

    // Register Custom Post Type
    function register_team_post_type() {

        $labels = array(
            'name'                  => _x( 'Team', 'Post Type General Name', THEME_TD ),
            'singular_name'         => _x( 'Team Member', 'Post Type Singular Name', THEME_TD ),
            'menu_name'             => __( 'Team', THEME_TD ),
            'name_admin_bar'        => __( 'Team Member', THEME_TD ),
            'archives'              => __( 'Item Archives', THEME_TD ),
            'attributes'            => __( 'Item Attributes', THEME_TD ),
            'parent_item_colon'     => __( 'Parent Item:', THEME_TD ),
            'all_items'             => __( 'All Items', THEME_TD ),
            'add_new_item'          => __( 'Add New Item', THEME_TD ),
            'add_new'               => __( 'Add New', THEME_TD ),
            'new_item'              => __( 'New Item', THEME_TD ),
            'edit_item'             => __( 'Edit Item', THEME_TD ),
            'update_item'           => __( 'Update Item', THEME_TD ),
            'view_item'             => __( 'View Item', THEME_TD ),
            'view_items'            => __( 'View Items', THEME_TD ),
            'search_items'          => __( 'Search Item', THEME_TD ),
            'not_found'             => __( 'Not found', THEME_TD ),
            'not_found_in_trash'    => __( 'Not found in Trash', THEME_TD ),
            'featured_image'        => __( 'Featured Image', THEME_TD ),
            'set_featured_image'    => __( 'Set featured image', THEME_TD ),
            'remove_featured_image' => __( 'Remove featured image', THEME_TD ),
            'use_featured_image'    => __( 'Use as featured image', THEME_TD ),
            'insert_into_item'      => __( 'Insert into item', THEME_TD ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', THEME_TD ),
            'items_list'            => __( 'Items list', THEME_TD ),
            'items_list_navigation' => __( 'Items list navigation', THEME_TD ),
            'filter_items_list'     => __( 'Filter items list', THEME_TD ),
        );
        $rewrite = array(
            'slug'                  => 'team',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => false,
        );
        $args = array(
            'label'                 => __( 'Team', THEME_TD ),
            'description'           => __( 'This is Team pages', THEME_TD ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'thumbnail', 'revisions', 'editor'),
            'taxonomies'            => array(),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-groups',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
//            'rewrite'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
        );
        register_post_type( 'team', $args );

    }

    function add_twig_helpers( $twig ) {
        /* this is where you can add your own functions to twig */

        $function = new Twig_SimpleFunction('menu_name', function ($theme_location) {

            return $this->get_theme_menu_name($theme_location);
        });
        $twig->addFunction($function);

        $function = new Twig_SimpleFunction('format_website_url', function ($url) {

            $url = preg_replace('#^https?://#', '', rtrim($url,'/'));
            return $url;
        });
        $twig->addFunction($function);


        return $twig;
    }

    function post_per_page_control( $query ) {
        if ( is_admin() || ! $query->is_main_query() )
            return;

        // For archive.You can omit this
        if ( is_post_type_archive('case_study') || is_post_type_archive('team') ) {
            //control the numbers of post displayed/listed (eg here 10)
            $query->set( 'posts_per_page', 30 );
            return;
        }

    }

}

new JmSite();