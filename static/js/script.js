"use strict";

var $j = jQuery.noConflict();


// Show Enquiry Pop Up
function showEnquirePopUp() {
    $j(".enquire-pop-up").css({ "transform": "translateX(0)" });
    $j("body").addClass("fixed-scroll");
}


// Show Enquiry Pop Up thanks message
function showEnquireThanksPopUp() {
    $j(".enquire-pop-up").css({ "transform": "translateX(-100%)" });
    $j(".enquire-thanks-pop-up").css({ "transform": "translateX(0)" });

    setTimeout(function () {
        $j(".enquire-pop-up").css({ "transition": "none" });
        $j(".enquire-pop-up").css({ "transform": "translateX(100%)" });

    }, 500);
    setTimeout(function () {
        $j(".enquire-pop-up").css({ "transition": "transform 0.5s" });
    }, 1000);
}


// Show Newsletter sign up pop up thanks message
function showSignUpPopUp() {
    $j(".sign-up-pop-up").css({ "transform": "translateX(0)" });
    $j("body").addClass("fixed-scroll");
}


// Show Newsletter Thanks sign up pop up
function showSignUpThanksPopUp() {
    $j(".sign-up-pop-up").css({ "transform": "translateX(-100%)" });
    $j(".sign-up-thanks-pop-up").css({ "transform": "translateX(0)" });

    setTimeout(function () {
        $j(".sign-up-pop-up").css({ "transition": "none" });
        $j(".sign-up-pop-up").css({ "transform": "translateX(100%)" });

    }, 500);
    setTimeout(function () {
        $j(".sign-up-pop-up").css({ "transition": "transform 0.5s" });
    }, 1000);
}


// Show Request Publication pop up
function showRequestPublicationPopUp() {
    $j(".request-publication-pop-up").css({ "transform": "translateX(0)" });
    $j("body").addClass("fixed-scroll");
}


// Show Request Publication pop up thanks message
function showRequestPublicationThanksPopUp() {
    $j(".request-publication-pop-up").css({ "transform": "translateX(-100%)" });
    $j(".request-publication-thanks-pop-up").css({ "transform": "translateX(0)" });

    setTimeout(function () {
        $j(".request-publication-pop-up").css({ "transition": "none" });
        $j(".request-publication-pop-up").css({ "transform": "translateX(100%)" });

    }, 500);
    setTimeout(function () {
        $j(".request-publication-pop-up").css({ "transition": "transform 0.5s" });
    }, 1000);
}


// Load more articles funtcion
function loadMoreArticles(articleClass) {
    $j(articleClass + ":hidden").slice(0, 3).css('display', 'block');
    $j("#loadMore").on('click', function (e) {
        e.preventDefault();
        $j(articleClass + ":hidden").slice(0, 3).slideDown();
        if ($j(articleClass + ":hidden").length == 0) {
            $j("#loadMore").parent().parent().slideUp();
        }
    });
}

function isotopCategoryFilter() {

    // init Isotope
    const $grid = $j('.case-studies__grid').isotope({
        itemSelector: '.case-studies__item',
        layoutMode: 'fitRows',
        getSortData: {
            category: '[data-category]'
        }
    });

    // bind filter button click
    $j('#category_filter').on('click', '.category-filter__list__item', function () {
        const filterValue = $j(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });


    // change is-checked class on buttons
    $j('.category-filter__list').each(function (i, buttonGroup) {
        const $buttonGroup = $j(buttonGroup);
        $buttonGroup.on('click', '.category-filter__list__item', function () {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $buttonGroup.find('input').removeAttr('checked');
            $j(this).addClass('is-checked').find('input').attr('checked');
        });
    });

    // set height in absolute element
    const arr = [];
    $j('.case-studies__item').each(function (i, but) {
        arr.push(but.offsetHeight);
    });
    const heighestEl = Math.max.apply(null, arr);
    $j('.case-studies__item').css({ minHeight: heighestEl });
}

// Show Request ebook pop up
function showRequestEbookPopUp() {
    $j(".request-ebook-pop-up").css({ "transform": "translateX(0)" });
    $j("body").addClass("fixed-scroll");
}

// Show Request ebook pop up thanks message
function showRequestEbookThanksPopUp() {
    $j(".request-ebook-pop-up").css({ "transform": "translateX(-100%)" });
    $j(".request-ebook-thanks-pop-up").css({ "transform": "translateX(0)" });

    setTimeout(function () {
        $j(".request-ebook-pop-up").css({ "transition": "none" });
        $j(".request-ebook-pop-up").css({ "transform": "translateX(100%)" });

    }, 500);
    setTimeout(function () {
        $j(".request-ebook-pop-up").css({ "transition": "transform 0.5s" });
    }, 1000);
}

$j(document).ready(function () {

    // Isotope
    if ($j('.case-studies__grid').length) {
        isotopCategoryFilter();
    }

    // Smooth scrolling to internal links
    $j('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $j(this.hash);
                target = target.length ? target : $j('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $j('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function () {
                        // Callback after animation
                        // Must change focus!
                        var $target = $j(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });


    // Mobile menu toggle handler
    $j(".toggle-btn").on("click", function (e) {
        e.preventDefault();
        $j(".header__toggle").toggleClass("header__toggle--open");
        $j(".header__nav").toggleClass("header__nav--open");
        $j("body").toggleClass("fixed-scroll");
    });


    // Header dropdown handler for mobile devices
    $j('.dropdown__toggle').on('click', function () {

        if ($j(window).width() > 991) {
            return
        }
        $j(this).next().slideToggle(280);
    });


    // Header dropdown handler for desktop devices
    $j(".navbar__dropdown").hover(
        function () {

            if ($j(window).width() < 992) {
                return
            }
            $j(this).find(".dropdown__items").stop(true, true).slideDown('medium');
        },
        function () {

            if ($j(window).width() < 992) {
                return
            }
            $j(this).find(".dropdown__items").stop(true, true).slideUp('medium');
        });


    // Close mobile menu on resize
    $j(window).resize(function () {
        if ($j(window).width() > 991 && $j(".header__toggle").hasClass("header__toggle--open")) {
            $j(".header__toggle").removeClass("header__toggle--open");
            $j(".header__nav").removeClass("header__nav--open");
            $j("body").removeClass("fixed-scroll");
        }
    });


    // Show enquire pop-up
    $j("button.enquire-now").on("click", function (e) {
        e.preventDefault();
        $j("button.enquire-now").css({ "right": "-60px" });
        showEnquirePopUp();
    });


    // Close pop-up
    $j(".pop-up__close-btn").on("click", function (e) {
        e.preventDefault();

        $j(this).parent().parent().css({ "transform": "translateX(100%)" });
        $j("button.enquire-now").css({ "right": "0" });

        $j("body").removeClass("fixed-scroll");
    });


    // Add youtube video to page
    $j(".video-section").click(function () {
        var video = '<iframe width="660" height="370" src="' + $j(this).attr('data-video') + '" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe>';
        $j(this).addClass('video-section--play');
        $j(".video-container", this).append(video);
    });


    // Init load-more btn for "latest news articles" & "resources" pages
    if ($j(".news-article").length != 0) {
        loadMoreArticles(".news-article");
    }

    if ($j(".resources-article").length != 0) {
        loadMoreArticles(".resources-article");
    }


    // Initializing popup
    $j('.popup-video').magnificPopup({
        type: 'iframe'
    });

});
