<?php

/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

if ($post->post_type === 'case_study') {

    $term_obj_list = get_the_terms( $post->ID, 'case_study-category' );
    $term_ids = wp_list_pluck($term_obj_list, 'term_id');

    $args = array(
        'post_type' => 'case_study',
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'orderby' => 'rand',
        'post__not_in' => array( $post->ID ),
        'tax_query' => array(
            array(
                'taxonomy' => 'case_study-category',
                'field'    => 'term_id',
                'terms'    => $term_ids,
            ),
        )
    );
    $context['similar_case_studies'] = Timber::get_posts($args);
}

if (post_password_required($post->ID)) {
    Timber::render('single-password.twig', $context);
} else {
    Timber::render(array('single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig'), $context);
}
